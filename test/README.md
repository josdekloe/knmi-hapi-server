HAPI 2.1.1

```
$ curl "http://localhost:8080/hapi/capabilities"
$ curl "http://localhost:8080/hapi/catalog"
$ curl "http://localhost:8080/hapi/info?id=hp30_index_PT30M"
$ curl "http://localhost:8080/hapi/data?id=hp30_index_PT30M&time.min=2000-01-01T00:00:00Z&time.max=2000-01-02T00:00:00Z"
```

Extended HAPI 2.1.1

```
$ curl -X POST -H "Content-Type: application/json" -d @dataset_hp30_index_PT30M.json http://localhost:8080/api/datasets
$ curl -X POST -H "Content-Type: application/json" -d @data_hp30_index.json http://localhost:8080/api/dataset
$ curl -X DELETE "http://localhost:8080/api/data?id=hp30_index_PT30M&time.min=2000-01-01T12:00:00Z&time.max=2000-01-02T00:00:00Z"
$ curl -X DELETE "http://localhost:8080/api/dataset?id=hp30_index_PT30M"
```

HAPI 3.1.0

```
$ curl "http://localhost:8080/hapi/about"
$ curl "http://localhost:8080/hapi/capabilities"
$ curl "http://localhost:8080/hapi/catalog"
$ curl "http://localhost:8080/hapi/info?dataset=hp30_index_PT30M"
$ curl "http://localhost:8080/hapi/data?dataset=hp30_index_PT30M&start=2000-01-01T00:00:00Z&stop=2000-01-02T00:00:00Z"
```

Extended HAPI 3.1.0

```
$ curl -X POST -H "Content-Type: application/json" -d @about.json "http://localhost:8080/api/about?key=xxxx"
$ curl -X POST -H "Content-Type: application/json" -d @dataset_hp30_index_PT30M.json "http://localhost:8080/api/datasets?key=xxxx"
$ curl -X POST -H "Content-Type: application/json" -d @data_hp30_index.json "http://localhost:8080/api/dataset?key=xxxx"
$ curl -X POST -H "Content-Type: application/json" -d @dataset_xray_flux_rt.json "http://localhost:8080/api/datasets?key=xxxx"
$ curl -X DELETE "http://localhost:8080/api/data?dataset=hp30_index_PT30M&start=2000-01-01T12:00:00Z&stop=2000-01-02T00:00:00Z&key=xxxx"
$ curl -X DELETE "http://localhost:8080/api/dataset?dataset=hp30_index_PT30M&key=xxxx"
```

