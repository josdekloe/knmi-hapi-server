#!/usr/bin/env python3
from data.ingestors import ingest_tools
from data.accessors import silso_sunspot_number
from data.data import resample_rdata

def ingest_silso_sunspots():
    filenames = silso_sunspot_number.download()
    for filename in filenames:
        print(filename)
        df = silso_sunspot_number.txt_to_dataframe(filename,
                                                   convert_column_names=True)
        df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")
        parameters = ['time', 'sunspot_number']
        data = df[parameters].values.tolist()
        ingest_tools.store('sunspots_silso', parameters, data, update=True)

    resample_rdata({'id': 'sunspots_silso'})

if __name__ == "__main__":
    ingest_silso_sunspots()