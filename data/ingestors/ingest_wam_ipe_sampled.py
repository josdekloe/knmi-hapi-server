import os
import pandas as pd
import xarray as xr
from data.ingestors import ingest_tools

def ingest_wam_ipe_sampled(input_dir='/Volumes/datasets/wam-ipe/sampled',
						   output_type='ipe',
						   sat = "Swarm C",
						   t0 = pd.to_datetime("2022-01-01", utc=True),
						   t1 = pd.to_datetime("2023-01-05", utc=True))

	if output_type == 'ipe':
		parameters = ['latitude',
				  	'longitude',
				  	'altitude',
				  	'electron_density',
				  	'electron_temperature',
				  	'VTEC_above',
				  	'eastward_exb_velocity',
				  	'northward_exb_velocity',
				  	'upward_exb_velocity']
	elif output_type == 'gsm':
		parameters = ['latitude',
				  	'longitude',
				  	'altitude',
				  	'mass_density',
				  	'temp_neutral',
				  	'u_neutral',
				  	'v_neutral',
				  	'w_neutral']
	else:
		print("Unknown sampled WAM-IPE output type: ", output_type)

	allparameters = ['time', *parameters]
	satletter = sat[-1:]

	# Loop over the days
	days = pd.date_range(t0, t1, freq='1D')
	for daystart, dayend in zip(days[:], days[1:]):
		in_file = f"{input_dir}/{output_type}_swarm{satletter.lower()}_{daystart.strftime('%Y%m%d')}.nc"
		if os.path.isfile(in_file):
			print(in_file)
			with xr.open_dataset(in_file) as xrdata:
				df = xrdata.to_dataframe()
				df['time'] = df.index.strftime("%Y-%m-%dT%H:%M:%SZ")

				if output_type == 'ipe':
					df['electron_density'] = df['electron_density'] * 1e-6 # Conversion from 1/m3 to 1/cm3

				data = df[allparameters].values.tolist()
				id = f'wam_ipe_ne_swarm{satletter.lower()}_PT10S'
				print("PT10S")
				ingest_tools.store(
					id,
					allparameters,
					data,
					update=True,
					api='api_url'
				)

				# Now process the lower cadences of high-rate (< 1 orbital period) data
				cadences = ['PT10S', 'PT30S', 'PT3M', 'PT6M', 'PT15M']
				for cadence in cadences:
					print(cadence)
					df_resampled = df[parameters].resample(pd.to_timedelta(cadence)).mean()
					df_resampled['time'] = df_resampled.index.strftime("%Y-%m-%dT%H:%M:%SZ")
					data = df_resampled[allparameters].values.tolist()
					id = f'wam_ipe_ne_swarm{satletter.lower()}_' + cadence
					ingest_tools.store(
						id,
						allparameters,
						data,
						update=True,
						api='api_url'
						)

if __name__ == "__main__":
	format = "%(asctime)s [%(levelname)s] %(message)s"
	logging.basicConfig(format=format, level=logging.INFO,
						datefmt="%Y-%m-%d %H:%M:%S")
	arguments = sys.argv[1:]
	times = ingest_tools.parse_date_args(arguments)
	if len(arguments) == 0:
		logging.info("Ingesting default sampled WAM-IPE data")
		ingest_wam_ipe_sampled()
	elif times:
		ingest_wam_ipe_sampled(t0=times[0], t1=times[1])
		continue
	else:
		continue

