#!/usr/bin/env python3
from datastore.accessors import swarm_diss

for data_type in ['MODx_SC', 'SP3xCOM', 'IPDxIRR', 'DNSxACC', 'DNSxPOD', 'NE']:
    for sat in ['Swarm A', 'Swarm B', 'Swarm C', 'Swarm AC', 'GRACE C', 'CHAMP']:
        files = swarm_diss.download(sat=sat, data_type=data_type)
