import os
import glob
import tarfile
import numpy as np
import pandas as pd
import xarray as xr


class wam_ipe_run():
    variable_collections = {'gsm10': {'coords': ["lon", "lat", "x03"],
                                      'vars': ["height",
                                               "temp_neutral",
                                               "O_Density",
                                               "O2_Density",
                                               "N2_Density",
                                               "u_neutral",
                                               "v_neutral",
                                               "w_neutral"]},
                            'gsm05': {'coords': ["lon", "lat"],
                                      'vars': ["den400", "ON2"]},
                            'ipe10': {'coords': ["lon", "lat", "alt"],
                                      'vars': ["O_plus_density",
                                               "H_plus_density",
                                               "He_plus_density",
                                               "N_plus_density",
                                               "NO_plus_density",
                                               "O2_plus_density",
                                               "N2_plus_density",
                                               "ion_temperature",
                                               "electron_temperature",
                                               "eastward_exb_velocity",
                                               "northward_exb_velocity",
                                               "upward_exb_velocity"]},
                            'ipe05': {'coords': ["lon", "lat", "alt"],
                                      'vars': ["tec", "NmF2", "HmF2"]}}

    collection_variables = {}
    for key in variable_collections:
        for var in variable_collections[key]['vars']:
            collection_variables[var] = key

    runinfo = None

    def __init__(self, runinfo):
        self.runinfo = runinfo

    def unpack_run(self):
        run_dir = self.runinfo['run_dir']
        cache_dir = self.runinfo['cache_dir']
        tar_files = sorted(glob.glob(run_dir + '/*.tar'))
        for tar_file in tar_files:
            try:
                with open(tar_file, 'rb') as fh:
                    tf = tarfile.TarFile(fileobj = fh)
                    filenames = tf.getnames()
                    for ncfile in filenames:
                        if not os.path.isfile(f"{cache_dir}{ncfile}"):
                            print(f"Extracting {ncfile} from {tar_file}")
                            tf.extract(ncfile, path=cache_dir)
            except tarfile.ReadError:
                print(f"tarfile read error on {tar_file}")

    def tarfiles_for_run(self):
        starttime = self.runinfo['run_datetime'] - pd.to_timedelta(3, 'H')
        endtime  = self.runinfo['run_datetime']  + pd.to_timedelta(2, 'D')
        times_60 = pd.date_range(starttime, endtime, freq='1H')
        tarfiles = []
        for time in times_60:
            tarfilename = f"{self.runinfo['run_dir']}wfs.t{self.runinfo['run_datetime'].hour:02d}z.{time.strftime('%Y%m%d')}_{time.strftime('%H')}.tar"
            tarfiles.append({'filename': tarfilename, 'available': os.path.isfile(tarfilename)})
        self.tarfiles = pd.DataFrame(data=tarfiles)
        return self.tarfiles

    def ncfiles_for_run(self, datatype):
        starttime = run.runinfo['run_datetime'] - pd.to_timedelta(3, 'H')
        endtime  = run.runinfo['run_datetime']  + pd.to_timedelta(2, 'D')
        ncfiles = []
        if datatype == 'gsm10' or datatype == 'ipe10':
            times = pd.date_range(starttime + pd.to_timedelta('10min'), endtime, freq='10min')
            for time in times:
                ncfilename = f"{self.runinfo['cache_dir']}wfs.t{self.runinfo['run_datetime'].hour:02d}z.{datatype}.{time.strftime('%Y%m%d')}_{time.strftime('%H%M%S')}.nc"
                ncfiles.append({'name': ncfilename, 'available': os.path.isfile(ncfilename)})
        elif datatype == 'gsm05' or datatype == 'ipe05':
            times = pd.date_range(starttime + pd.to_timedelta('5min'), endtime, freq='5min')
            for time in times:
                ncfilename = f"{run['cache_dir']}wfs.t{run['run_datetime'].hour:02d}z.{datatype}.{time.strftime('%Y%m%d')}_{time.strftime('%H%M%S')}.nc"
                ncfiles.append({'name': ncfilename, 'available': os.path.isfile(ncfilename)})
        return pd.DataFrame(data=ncfiles)


    def extract_xarray(self, output_type):
        # output_type is e.g. ipe10, ipe05, gsm10, gsm05
        variables = self.variable_collections[output_type]
        if output_type == 'gsm10':
            nheights = 150
        else:
            nheights = 58
        nlons = 91
        nlats = 91
        times = []
        ncfiles = sorted(glob.glob(self.runinfo['cache_dir'] + f'/*{output_type}*.nc'))
        if len(ncfiles) == 0:
            print(f"No NetCDF files found in {cache_dir}")
            return None

        out_coords_tuple = tuple('time') + tuple(variables['coords'])
        # Set up output data array
        if output_type == 'ipe10' or output_type == 'gsm10': # 3D data
            array_out={var: np.zeros((len(ncfiles), nheights, nlons, nlats), dtype='float32') for var in variables['vars']}
        elif output_type == 'ipe05' or output_type == 'gsm05':
            array_out={var: np.zeros((len(ncfiles), nlons, nlats), dtype='float32') for var in variables['vars']}
        else:
            print("Unknown output type: ", output_type)

        # Loop over the NetCDF files
        for i, ncfile in enumerate(ncfiles):
            # Parse time/data from netcdf filename
            times.append(pd.to_datetime(ncfile.split('.')[-2].replace('_', 'T'), utc=True))

            # Open the NetCDF file in xarray
            xrdata_in = xr.open_dataset(ncfile)

            if output_type == 'ipe10' or output_type == 'gsm10':
                if i==0: # fill the coordinate vars only once
                    if output_type == 'ipe10':
                        altitudes = xrdata_in['alt'].data
                    if output_type == 'gsm10':
                        altitudes = xrdata_in['x03'].data
                    latitudes = xrdata_in['lat'].data
                    longitudes = np.arange(0.0, 361.0, 4.0, dtype='float32')
                for var in variables['vars']: # fill the variables
                    array_out[var][i,:,:,:] = np.concatenate((xrdata_in[var].data, xrdata_in[var].data[:,:,0].reshape(nheights,nlats,1)), axis=2)
            else:
                if i==0:
                    latitudes = xrdata_in['lat'].data
                    longitudes = np.arange(0.0, 361.0, 4.0, dtype='float32')
                for var in variables['vars']:
                    array_out[var][i,:,:] = np.concatenate((xrdata_in[var].data, xrdata_in[var].data[:,0].reshape(91,1)), axis=1)
            xrdata_in.close()

        timeseries = pd.Series(times)
        seconds = (( timeseries - timeseries[0] ) / pd.to_timedelta(1, 'sec')).values
        if output_type == 'ipe10':
            data_vars = {var: (('time', 'alt', 'lat', 'lon'), array_out[var]) for var in variables['vars']}
            coords = {'time': timeseries.values, 'alt': altitudes, 'lat': latitudes, 'lon': longitudes}
            xarray_out = xr.Dataset(data_vars=data_vars, coords=coords)
        elif output_type == 'gsm10':
            for var in variables['vars']:
                array_out[var][:,:,0,:] = np.mean(array_out[var][:,:,1,:]) # Set min and max lats (polar points) which are usually set to zero or Nan
                array_out[var][:,:,90,:] = np.mean(array_out[var][:,:,89,:])
            data_vars = {var: (('time', 'lev', 'lat', 'lon'), array_out[var]) for var in variables['vars']}
            coords = {'time': timeseries.values, 'x03': altitudes, 'lat': latitudes, 'lon': longitudes}
            xarray_out = xr.Dataset(data_vars=data_vars, coords=coords)
        elif output_type == 'gsm05':
            for var in variables['vars']:
                array_out[var][:,0,:] = np.mean(array_out[var][:,1,:]) # Set min and max lats (polar points) which are usually set to zero or Nan
                array_out[var][:,90,:] = np.mean(array_out[var][:,89,:])
            data_vars = {var: (('time', 'lat', 'lon'), array_out[var]) for var in variables['vars']}
            coords = {'time': timeseries.values, 'lat': latitudes, 'lon': longitudes}
            xarray_out = xr.Dataset(data_vars=data_vars, coords=coords)
        else:
            data_vars = {var: (('time', 'lat', 'lon'), array_out[var]) for var in variables['vars']}
            coords = {'time': timeseries.values, 'lat': latitudes, 'lon': longitudes}
            xarray_out = xr.Dataset(data_vars=data_vars, coords=coords)

        return xarray_out


class wam_ipe():

    def __init__(self, root_dir = '/Volumes/datasets/wam-ipe/', cache_dir = '/Volumes/datasets/wam-ipe/unpacked/'):
        runs = []
        # List the run directories
        # run dir paths look like this: /Volumes/datasets/wam-ipe/wfs.20211123/00
        run_dirs = sorted(glob.glob(f"{root_dir}wfs*/*"))
        for run_dir in run_dirs:
            run_dir = run_dir
            run_cache_dir = cache_dir + run_dir.replace(root_dir, '') + '/'
            path_pieces = run_dir.split("/")
            rundate = pd.to_datetime(path_pieces[-2].split(".")[-1]) # Get the run date
            runtime = pd.to_timedelta(int(path_pieces[-1]), unit='H') # Get the run time in hours

            # Now get the start and end time from the tar file names contained in the run dir
            # tar file paths look like this "/Volumes/datasets/wam-ipe/wfs.20211123/00/wfs.t00z.20211122_21.tar"
            # get the simulation start and end times from these file names
            tar_files = sorted(glob.glob(run_dir + '/*'))
            (date, hour) = tar_files[0].split("/")[-1].split(".")[-2].split("_")
            start_time = pd.to_datetime(date) + pd.to_timedelta(int(hour), 'H')
            (date, hour) = tar_files[-1].split("/")[-1].split(".")[-2].split("_")
            end_time = pd.to_datetime(date) + pd.to_timedelta(int(hour), 'H')

            # Construct the record
            runs.append({'run_datetime': pd.to_datetime(rundate+runtime),
                         'run_dir': run_dir + '/',
                         'cache_dir': run_cache_dir,
                         'cache_dir_exists': os.path.isdir(run_cache_dir),
                         'start_time': start_time,
                         'end_time': end_time})

        self.runs = pd.DataFrame(runs)
        self.root_dir = root_dir
        self.cache_dir = cache_dir


    def runs_for_time(self, time):
        frame = self.runs[(time > self.runs['start_time']) & (time < self.runs['end_time'])]
        frame = frame.assign(time_delta = (time - frame.loc[:, 'run_datetime'])/pd.to_timedelta(1, 'H'))
        return frame


