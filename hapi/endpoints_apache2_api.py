from flask import Flask, request, Response
from hapi import hapi_3_1_0 as hapi
from flask_cors import CORS

application = Flask(__name__)
CORS(application)
application.config['CORS_HEADERS'] = 'Content-Type'

# ------------------------------------------------------------------------------
# API endpoints
# ------------------------------------------------------------------------------

@application.route('/about', methods=['GET', 'POST'])
def api_about():
    if request.method == 'GET':
        response, status, mimetype = hapi.api_about(request.args, request.method, {})
    else:
        response, status, mimetype = hapi.api_about(request.args, request.method, request.json)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/capabilities', methods=['GET'])
def api_capabilities():
    response, status, mimetype = hapi.api_capabilities(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/datasets', methods=['GET', 'PUT', 'POST'])
def api_datasets():
    if request.method == 'GET':
        response, status, mimetype = hapi.api_datasets(request.args, request.method, {})
    else:
        response, status, mimetype = hapi.api_datasets(request.args, request.method, request.json)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/dataset', methods=['GET', 'PUT', 'DELETE', 'POST'])
def api_dataset():
    if request.method == 'GET' or request.method == 'DELETE':
        response, status, mimetype = hapi.api_dataset(request.args, request.method, {})
    else:
        response, status, mimetype = hapi.api_dataset(request.args, request.method, request.json)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/data', methods=['GET', 'DELETE'])
def api_data():
    if request.method == 'GET':
        response, status, mimetype = hapi.api_data(request.args, request.method)
    else:
        response, status, mimetype = hapi.api_data(request.args, request.method, request.json)
    return Response(response, status=status, mimetype=mimetype)

