from flask import Flask, request, Response
from hapi import hapi_3_1_0 as hapi
from flask_cors import CORS, cross_origin

application = Flask(__name__)
CORS(application)
application.config['CORS_HEADERS'] = 'Content-Type'

# ------------------------------------------------------------------------------
# HAPI endpoints
# ------------------------------------------------------------------------------

@application.route('/hapi/capabilities', methods=['GET'])
@cross_origin()
def hapi_capabilities():
    response, status, mimetype = hapi.hapi_capabilities(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/hapi/catalog', methods=['GET'])
@cross_origin()
def hapi_catalog():
    response, status, mimetype = hapi.hapi_catalog(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/hapi/info', methods=['GET'])
@cross_origin()
def hapi_info():
    response, status, mimetype = hapi.hapi_info(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/hapi/data', methods=['GET'])
@cross_origin()
def hapi_data():
    response, status, mimetype = hapi.hapi_data(request.args)
    return Response(response, status=status, mimetype=mimetype)

if hasattr(hapi, 'hapi_about'):
    @cross_origin()
    @application.route('/hapi/about', methods=['GET'])
    def hapi_about():
        response, status, mimetype = hapi.hapi_about(request.args)
        return Response(response, status=status, mimetype=mimetype)

# ------------------------------------------------------------------------------
# API endpoints
# ------------------------------------------------------------------------------

@application.route('/api/capabilities', methods=['GET'])
def api_capabilities():
    response, status, mimetype = hapi.api_capabilities(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/api/datasets', methods=['GET', 'PUT', 'POST'])
def api_datasets():
    if request.method == 'GET':
        response, status, mimetype = hapi.api_datasets(request.args, request.method, {})
    else:
        response, status, mimetype = hapi.api_datasets(request.args, request.method, request.json)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/api/dataset', methods=['GET', 'PUT', 'DELETE', 'POST'])
def api_dataset():
    if request.method == 'GET' or request.method == 'DELETE':
        response, status, mimetype = hapi.api_dataset(request.args, request.method, {})
    else:
        response, status, mimetype = hapi.api_dataset(request.args, request.method, request.json)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/api/data', methods=['GET', 'DELETE'])
def api_data():
    if request.method == 'GET':
        response, status, mimetype = hapi.api_data(request.args, request.method)
    else:
        response, status, mimetype = hapi.api_data(request.args, request.method, request.json)
    return Response(response, status=status, mimetype=mimetype)

if hasattr(hapi, 'api_about'):
    @application.route('/api/about', methods=['GET', 'POST'])
    def api_about():
        if request.method == 'GET':
            response, status, mimetype = hapi.api_about(request.args, request.method, {})
        else:
            response, status, mimetype = hapi.api_about(request.args, request.method, request.json)
        return Response(response, status=status, mimetype=mimetype)

