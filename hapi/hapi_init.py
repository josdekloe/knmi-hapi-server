import sys
import logging
sys.path.insert(0,"/var/www/knmi-hapi-server/")
from hapi import hapi_3_1_0 as hapi

format = '%(asctime)s [%(levelname).1s] %(message)s'
filename = '/var/www/knmi-hapi-server/hapi_server.key'
logging.basicConfig(format=format, filename=filename, level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')

hapi.init_db()
hapi.add_about()
hapi.add_authorization()
