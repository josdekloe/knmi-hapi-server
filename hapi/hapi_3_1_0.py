#!/usr/bin/env python3

# partial implementation of
# https://github.com/hapi-server/data-specification/blob/master/hapi-3.1.0/HAPI-data-access-spec-3.1.0.md

from datetime import datetime, timedelta
import pandas as pd
import logging
import json
from hapi.config import config
import traceback
import sqlite3
import psycopg2
from psycopg2.pool import ThreadedConnectionPool
import secrets
import string
import urllib

hapi_version = '3.1'
ISO_TIMEFMT = '%Y-%m-%dT%H:%M:%S.%fZ'


logging.basicConfig(format='%(asctime)s [%(levelname).1s] %(message)s',
                    level=logging.INFO,
                    datefmt='%Y-%m-%d %H:%M:%S')

api_status_codes = {
    "1200": {"http_code": 200, "status":{"code": 1200, "message": "HAPI 1200: OK"}},
    "1201": {"http_code": 200, "status":{"code": 1201, "message": "HAPI 1201: OK - no data"}},
    "1400": {"http_code": 400, "status":{"code": 1400, "message": "HAPI error 1400: user input error"}},
    "1401": {"http_code": 400, "status":{"code": 1401, "message": "HAPI error 1401: unknown API parameter name"}},
    "1402": {"http_code": 400, "status":{"code": 1402, "message": "HAPI error 1402: error in start"}},
    "1403": {"http_code": 400, "status":{"code": 1403, "message": "HAPI error 1403: error in stop"}},
    "1404": {"http_code": 400, "status":{"code": 1404, "message": "HAPI error 1404: start equal to or after stop"}},
    "1405": {"http_code": 400, "status":{"code": 1405, "message": "HAPI error 1405: time outside valid range"}},
    "1406": {"http_code": 400, "status":{"code": 1406, "message": "HAPI error 1406: unknown dataset id"}},
    "1407": {"http_code": 400, "status":{"code": 1407, "message": "HAPI error 1407: unknown dataset parameter"}},
    "1408": {"http_code": 400, "status":{"code": 1408, "message": "HAPI error 1408: too much time or data requested"}},
    "1409": {"http_code": 400, "status":{"code": 1409, "message": "HAPI error 1409: unsupported output format"}},
    "1410": {"http_code": 400, "status":{"code": 1410, "message": "HAPI error 1410: unsupported include value"}},
    "1411": {"http_code": 400, "status":{"code": 1411, "message": "HAPI error 1411: out of order or duplicate parameters"}},
    "1412": {"http_code": 400, "status":{"code": 1412, "message": "HAPI error 1412: unsupported resolve_references value"}},
    "1450": {"http_code": 400, "status":{"code": 1450, "message": "HAPI error 1450: dataset already exists"}},
    "1500": {"http_code": 500, "status":{"code": 1500, "message": "HAPI error 1500: internal server error"}},
    "1501": {"http_code": 500, "status":{"code": 1501, "message": "HAPI error 1501: upstream request error"}}
}

md_server = [
    {'name': 'id', 'type': 'string', 'required': True, 'key': True},
    {'name': 'title', 'type': 'string', 'required': True, 'key': False},
    {'name': 'contact', 'type': 'string', 'required': True, 'key': False},
    {'name': 'description', 'type': 'string', 'required': False, 'key': False},
    {'name': 'contactID', 'type': 'string', 'required': False, 'key': False},
    {'name': 'citation', 'type': 'string', 'required': False, 'key': False},
]

md_dataset = [
    {'name': 'id', 'type': 'string', 'required': True, 'key': True, 'updatable': False},
    {'name': 'startDate', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'stopDate', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'timeStampLocation', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
    {'name': 'cadence', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'sampleStartDate', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
    {'name': 'sampleStopDate', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
    {'name': 'description', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
    {'name': 'resourceURL', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
    {'name': 'resourceID', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
    {'name': 'creationDate', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'modificationDate', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'contact', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
    {'name': 'contactID', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
]

md_parameter = [
    {'name': 'name', 'type': 'string', 'required': True, 'key': True, 'updatable': False},
    {'name': 'type', 'type': 'string', 'required': True, 'key': False, 'updatable': False},
    {'name': 'length', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'size', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'units', 'type': 'string', 'required': True, 'key': False, 'updatable': True},
    {'name': 'coordinateSystemName', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'vectorComponents', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'fill', 'type': 'string', 'required': True, 'key': False, 'updatable': False},
    {'name': 'description', 'type': 'string', 'required': False, 'key': False, 'updatable': True},
    {'name': 'label', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
    {'name': 'bins', 'type': 'string', 'required': False, 'key': False, 'updatable': False},
]

md_relations = [
    {'name': 'id', 'type': 'string', 'required': True, 'key': True},
    {'name': 'relations', 'type': 'string', 'required': True, 'key': False},
]

md_authorization_key = [
    {'name': 'key', 'type': 'string', 'required': True, 'key': True},
    {'name': 'expirationDate', 'type': 'string', 'required': False, 'key': False},
    {'name': 'operationsList', 'type': 'string', 'required': False, 'key': False},
]

md_parameter_db = md_parameter.copy()
md_parameter_db.append({'name': 'datasetID', 'type': 'string', 'required': False, 'key': True})

def get_response(status, message, hapi=False):
    message['HAPI'] = hapi_version
    message['status'] = api_status_codes[status]['status']
    http_code = api_status_codes[status]['http_code']
    response = json.dumps(message, indent=4) + '\n'
    return response, http_code, 'application/json'


def get_csv_response(status, message, header, hapi=False):
    http_code = api_status_codes[status]['http_code']
    if header == {}:
        return message, http_code, 'text/csv'
    if type(header) != str:
        header['HAPI'] = hapi_version
        header['status'] = api_status_codes[status]['status']
        header = json.dumps(header, indent=4)
        lines = header.split('\n')
        header = ''
        for line in lines:
            header += '#' + line + '\n'
    else:
        header = header + '\n'
    return header + message, http_code, 'text/csv'


def parse_isoduration(s):
    def get_isosplit(s, split):
        if split in s:
            n, s = s.split(split)
        else:
            n = 0
        return n, s

    # Remove prefix
    s = s.split('P')[-1]

    # Step through letter dividers
    days, s = get_isosplit(s, 'D')
    _, s = get_isosplit(s, 'T')
    hours, s = get_isosplit(s, 'H')
    minutes, s = get_isosplit(s, 'M')
    seconds, s = get_isosplit(s, 'S')

    # Convert all to seconds
    dt = timedelta(days=int(days), hours=int(hours), minutes=int(minutes),
                   seconds=int(seconds))
    return int(dt.total_seconds())

def rows_to_dataframe(rows, parameter_names, keep_time_col=False, time_name='time'):
    df = pd.DataFrame.from_records(data=rows, columns=parameter_names)
    df.index = pd.to_datetime(df[time_name], utc=True)
    df = df.drop([time_name], axis=1)
    return df

def type_db(type):
    if type == 'string':
        return 'TEXT'
    if type == 'isotime':
        return 'TEXT'
    if type == 'double':
        return 'REAL'
    if type == 'integer':
        return 'INTEGER'
    return ''

tcp = None

def init_db_connection():
    global tcp
    if config['database_type'] == 'postgres':
        tcp = ThreadedConnectionPool(1, 40,
            host=config['database_host'],
            database=config['database_name'],
            user=config['database_user'],
            password=config['database_password'])
    return

def get_db_connection():
    con = None
    if config['database_type'] == 'postgres':
        con = tcp.getconn()
    elif config['database_type'] == 'sqlite':
        con = sqlite3.connect(config['database_path'])
    else:
        logging.error('invalid database type: ' + config['database_type'])
    return con

def release_db_connection(con):
    if config['database_type'] == 'postgres':
        tcp.putconn(con)
    elif config['database_type'] == 'sqlite':
        con.close()
    else:
        logging.error('invalid database type: ' + config['database_type'])

def get_db_placeholder():
    placeholder = '?'
    if config['database_type'] == 'postgres':
        placeholder = '%s'
    return placeholder

def init_db():
    init_db_connection()
    con = get_db_connection()
    cur = con.cursor()
    statement = create_create_statement('md_server', md_server)
    cur.execute(statement)
    statement = create_create_statement('md_dataset', md_dataset)
    cur.execute(statement)
    statement = create_create_statement('md_parameter', md_parameter_db)
    cur.execute(statement)
    statement = create_create_statement('md_relations', md_relations)
    cur.execute(statement)
    statement = create_create_statement('md_authorization_key', md_authorization_key)
    cur.execute(statement)
    con.commit()
    release_db_connection(con)

def create_create_statement(dataset, columns):
    keys = []
    for column in columns:
        if 'key' in column and column['key']:
            keys.append(column['name'])
    statement = 'CREATE TABLE IF NOT EXISTS ' + dataset + '('
    statement += ', '.join([(column['name'] + ' ' + type_db(column['type']))
                            for column in columns])
    if keys != []:
        statement += ', PRIMARY KEY (' + ', '.join(keys) + ')'
    statement += ');'
    return statement

# note: this code must be located after defining create_create_statement
#       since init_db() calls that function!
try:
    logging.info("init db")
    tcp = init_db()
except:
    logging.info(traceback.format_exc())

def create_insert_statement(dataset, columns, parameters=None, replace=False):
    if config['database_type'] == 'postgres':
        keys = []
        if 'time' in columns:
            keys.append('time')
        if 'timetag_issue' in columns:
            keys.append('timetag_issue')
        statement = 'INSERT'
        statement += ' INTO ' + dataset + ' ( '
        statement += ' , '.join(columns)
        statement += ' ) VALUES ( '
        statement += ' , '.join([get_db_placeholder() for column in columns])
        if len(keys) == 0:
            statement += ');'
        else:
            statement += ' ) ON CONFLICT (' + ' , '.join(keys) + ') '
            if replace:
                statement += 'DO UPDATE SET '
                statement += ' , '.join([f'{column} = EXCLUDED.{column}' for column in columns])
                statement += ";"
            else:
                statement += 'DO NOTHING;'
    else:
        statement = 'REPLACE' if replace else 'INSERT OR IGNORE'
        statement += ' INTO ' + dataset + ' ( '
        statement += ' , '.join(columns)
        statement += ' ) VALUES ( '
        statement += ' , '.join([get_db_placeholder() for column in columns])
        statement += ' );'
    return statement


# ------------------------------------------------------------------------------
# HAPI endpoints implementation
# ------------------------------------------------------------------------------

def hapi_about(args):
    try:
        status, reply = get_about(args)
        return get_response(status, reply, hapi=True)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {}, hapi=True)


def hapi_capabilities(args):
    try:
        status, reply = get_capabilities(args)
        return get_response(status, reply, hapi=True)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {}, hapi=True)


def hapi_catalog(args):
    format = '%(asctime)s [%(levelname).1s] %(message)s'
    logging.basicConfig(format=format, level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')
    try:
        status, reply = get_datasets(args, 'catalog')
        return get_response(status, reply, hapi=True)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {}, hapi=True)


def hapi_info(args):
    try:
        status, reply = get_dataset(args)
        return get_response(status, reply, hapi=True)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {}, hapi=True)


def hapi_data(args):
    try:
        status, format, reply, reply_header = get_data(args)
        if format == 'json':
            return get_response(status, reply, hapi=True)
        if format == 'csv':
            return get_csv_response(status, reply, reply_header, hapi=True)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {}, hapi=True)


# ------------------------------------------------------------------------------
# API endpoints implementation
# ------------------------------------------------------------------------------

def api_about(args, method, data=None):
    try:
        if method == 'GET':
            status, reply = get_about(args)
            return get_response(status, reply)
        if method == 'POST':
            status, reply = set_about(data, args)
            return get_response(status, reply)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {})


def api_capabilities(args):
    try:
        status, reply = get_capabilities(args)
        return get_response(status, reply)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {})


def api_datasets(args, method, data=None):
    try:
        if method == 'GET':
            status, reply = get_datasets(args, 'datasets')
            return get_response(status, reply)
        if method == 'PUT':
            status, reply = add_dataset(data, args, replace=True)
            return get_response(status, reply)
        if method == 'POST':
            status, reply = add_dataset(data, args)
            return get_response(status, reply)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {})


def api_dataset(args, method, data=None):
    try:
        if method == 'GET':
            status, reply = get_dataset(args)
            return get_response(status, reply)
        if method == 'PUT':
            status, reply = add_data(data, args, replace=True)
            return get_response(status, reply)
        if method == 'DELETE':
            status, reply = remove_dataset(args)
            return get_response(status, reply)
        if method == 'POST':
            status, reply = add_data(data, args)
            return get_response(status, reply)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {})

def api_data(args, method):
    try:
        if method == 'GET':
            status, format, reply, reply_header = get_data(args)
            if format == 'json':
                return get_response(status, reply)
            elif format == 'csv':
                return get_csv_response(status, reply, reply_header)
        if method == 'DELETE':
            status, reply = remove_data(args)
            return get_response(status, reply)
    except:
        logging.info(traceback.format_exc())
        return get_response('1500', {})


def get_about(args):
    allowed_args = []
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    con = get_db_connection()
    cur = con.cursor()
    statement = 'SELECT * FROM md_server'
    cur.execute(statement)
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    if rows:
        reply = {}
        row = rows[0]

        reply['id'] = row[0]
        reply['title'] = row[1]
        reply['contact'] = row[2]
        reply['description'] = row[3]
        reply['contactID'] = row[4]
        reply['citation'] = row[5]
        return '1200', reply
    else:
        # seems the database is still empty
        # which may happen when hapi_init.py is run for the first time
        return '1201', {}

def set_about(server, args):
    allowed_args = ['key']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}
    if 'key' in args:
        key = args['key']
    else:
        logging.error('required parameter missing: key')
        return '1400', {}
    if not is_authorized(key, 'set_about'):
        logging.error('not authorized')
        return '1400', {}

    server_names = []
    server_values = []

    names = []
    values = []
    for item in md_server:
        name = item['name']
        value = ""
        if name in server:
            value = server[name]
        elif item['required']:
            logging.error('required parameter missing: {}'.format(name))
            return '1400', {}
        names.append(name)
        values.append(value)

    server_names = names
    server_values.append(tuple(values))

    con = get_db_connection()
    cur = con.cursor()
    statement = 'DELETE FROM md_server'
    cur.execute(statement)
    statement = create_insert_statement('md_server', server_names)
    cur.executemany(statement, server_values)
    con.commit()
    release_db_connection(con)

    return '1200', {}


def add_about():

    server = { 'id': 'HAPIServer' + hapi_version, 'title': 'HAPI Server ' + hapi_version, 'contact': '-' }

    server_names = []
    server_values = []

    names = []
    values = []
    for item in md_server:
        name = item['name']
        value = ""
        if name in server:
            value = server[name]
        elif item['required']:
            logging.error('required parameter missing: {}'.format(name))
            return '1400', {}
        names.append(name)
        values.append(value)

    server_names = names
    server_values.append(tuple(values))

    con = get_db_connection()
    cur = con.cursor()
    statement = 'DELETE FROM md_server'
    cur.execute(statement)
    statement = create_insert_statement('md_server', server_names, md_server)
    cur.executemany(statement, server_values)
    con.commit()
    release_db_connection(con)

    return '1200', {}


def get_capabilities(args):
    allowed_args = []
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}
    reply = {}
    reply['outputFormats'] = ['csv', 'json']
    return '1200', reply


def get_datasets(args, target):
    logging.info('get_datasets()')
    allowed_args = []
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    logging.info('before get_db_connection()')
    con = get_db_connection()
    cur = con.cursor()
    statement = 'SELECT id, description FROM md_dataset ORDER BY id ASC'
    cur.execute(statement)
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    reply = []
    for row in rows:
        reply.append({'id': row[0], 'title': row[1]})

    return '1200', {target: reply}


def get_dataset(args):
    allowed_args = ['id', 'dataset', 'parameters']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}

    if 'dataset' in args:
        id = args['dataset']
    elif 'id' in args: # deprecated
        id = args['id']
    else:
        logging.error('required parameter missing: dataset')
        return '1400', {}

    parameters_request = []
    if 'parameters' in args:
        parameters_request = args['parameters'].split(',')

    if parameters_request != []:
        parameters_request.append('time')

    names = []
    for item in md_dataset:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ' , '.join(names)
    statement += ' FROM md_dataset WHERE id = ' + get_db_placeholder()

    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    if rows == []:
        return '1406', {}

    reply = {}

    for i in range(len(names)):
        reply[names[i]] = list(rows[0])[i]

    names = []
    for item in md_parameter:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ' , '.join(names)
    statement += ' FROM md_parameter WHERE datasetID = ' + get_db_placeholder()

    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    parameters = []
    for row in rows:
        parameter = {}
        for i in range(len(names)):
            value = list(row)[i]
            if md_parameter[i]['required'] or len(value) > 0:
                parameter[names[i]] = value
        if parameters_request == [] or parameter['name'] in parameters_request:
            parameters.append(parameter)

    reply['parameters'] = parameters

    statement = 'SELECT relations FROM md_relations WHERE id = '+ get_db_placeholder()
    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    if len(rows) != 0:
        reply['x_relations'] = json.loads(rows[0][0])

    return '1200', reply


def remove_dataset(args):
    allowed_args = ['id', 'dataset', 'key']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}
    if 'key' in args:
        key = args['key']
    else:
        logging.error('required parameter missing: key')
        return '1400', {}
    if not is_authorized(key, 'remove_dataset'):
        logging.error('not authorized')
        return '1400', {}
    if 'dataset' in args:
        id = args['dataset']
    elif 'id' in args: # deprecated
        id = args['id']
    else:
        logging.error('required parameter missing: dataset')
        return '1400', {}

    statement = 'SELECT relations FROM md_relations WHERE id = '+ get_db_placeholder()
    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    if len(rows) != 0:
        relations = json.loads(rows[0][0])
        for relation in relations:
            args_related = args.copy()
            args_related['dataset'] = relation['id']
            remove_dataset(args_related)

    con = get_db_connection()
    cur = con.cursor()
    statement = 'DROP TABLE IF EXISTS ' + id
    cur.execute(statement)
    statement = 'DELETE FROM md_dataset WHERE id = ' + get_db_placeholder()
    cur.execute(statement, (id,))
    statement = 'DELETE FROM md_parameter WHERE datasetID = ' + get_db_placeholder()
    cur.execute(statement, (id,))
    statement = 'DELETE FROM md_relations WHERE id = ' + get_db_placeholder()
    cur.execute(statement, (id,))
    con.commit()
    release_db_connection(con)

    return '1200', {}


def replace_dataset(dataset, args):
    keys = []
    key_values = []
    values = []
    names = []
    for item in md_dataset:
        name = item['name']
        value = ""
        if name in dataset:
            value = dataset[name]
            if item['key']:
                keys.append(name + '=' + get_db_placeholder())
                key_values.append(value)
            elif item['updatable']:
                names.append(name + '=' + get_db_placeholder())
                values.append(value)
    statement = 'UPDATE md_dataset SET '
    statement = statement + ' , '.join(names)
    statement = statement + ' WHERE '
    statement = statement + ' AND '.join(keys)
    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, tuple(values + key_values))
    con.commit()

    parameters = dataset['parameters']
    for parameter in parameters:
        keys = []
        key_values = []
        values = []
        names = []
        for item in md_parameter:
            name = item['name']
            value = ""
            if name in parameter:
                value = parameter[name]
                if item['key']:
                    keys.append(name + '=' + get_db_placeholder())
                    key_values.append(value)
                elif item['updatable']:
                    names.append(name + '=' + get_db_placeholder())
                    values.append(value)
        statement = 'UPDATE md_parameter SET '
        statement = statement + ' , '.join(names)
        statement = statement + ' WHERE '
        statement = statement + ' AND '.join(keys)
        con = get_db_connection()
        cur = con.cursor()
        cur.execute(statement, tuple(values + key_values))
        con.commit()
    release_db_connection(con)

    relations = None
    if 'x_relations' in dataset:
        relations = dataset['x_relations']
        del dataset['x_relations']

    if relations != None:
        for relation in relations:
            if relation['type'] == 'link':
                continue
            dataset_related = dataset.copy()
            for field in relation:
                if field in dataset_related:
                    dataset_related[field] = relation[field]
            status, reply = replace_dataset(dataset_related, args)
            if status != '1200':
                return status, reply
    return '1200', {}


def add_dataset(dataset, args, replace=False):
    allowed_args = ['key']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}
    if 'key' in args:
        key = args['key']
    else:
        logging.error('required parameter missing: key')
        return '1400', {}
    if not is_authorized(key, 'add_dataset'):
        logging.error('not authorized')
        return '1400', {}

    con = get_db_connection()
    cur = con.cursor()
    statement = 'SELECT id FROM md_dataset WHERE id = ' + get_db_placeholder()
    cur.execute(statement, (dataset['id'],))
    rows = cur.fetchall()
    if rows != []:
        # dataset already exists - misuse error message for now
        if replace:
            release_db_connection(con)
            return replace_dataset(dataset, args)
        else:
            release_db_connection(con)
            logging.error('dataset already exists: {}'.format(dataset['id']))
            return '1450', {}

    relations = None
    if 'x_relations' in dataset:
        relations = dataset['x_relations']
        del dataset['x_relations']

    dataset_names = []
    dataset_values = []

    names = []
    values = []
    for item in md_dataset:
        name = item['name']
        value = ""
        if name in dataset:
            value = dataset[name]
        elif item['required']:
            logging.error('required parameter missing: {}'.format(name))
            return '1400', {}
        if name == 'creationDate':
            value = datetime.today().strftime(ISO_TIMEFMT)
        names.append(name)
        values.append(value)

    dataset_names = names
    dataset_values.append(tuple(values))

    parameters = dataset["parameters"]

    parameter_names = []
    parameter_values = []

    for parameter in parameters:
        names = []
        values = []
        for item in md_parameter:
            name = item["name"]
            value = ""
            if name in parameter:
                value = parameter[name]
            elif item['required']:
                logging.error('required parameter missing: {}'.format(name))
                return '1400', {}
            names.append(name)
            values.append(value)
        names.append('datasetID')
        values.append(dataset['id'])

        parameter_names = names
        parameter_values.append(tuple(values))

    statement = create_insert_statement('md_dataset', dataset_names, md_dataset)
    cur.executemany(statement, dataset_values)
    statement = create_insert_statement('md_parameter', parameter_names, md_parameter_db)
    cur.executemany(statement, parameter_values)
    statement = create_create_statement(dataset['id'], parameters)
    cur.execute(statement)
    con.commit()
    release_db_connection(con)

    if relations != None:
        for relation in relations:
            if relation['type'] == 'link':
                continue
            dataset_related = dataset.copy()
            for field in relation:
                if field in dataset_related:
                    dataset_related[field] = relation[field]
            status, reply = add_dataset(dataset_related, args, replace)
            if status != '1200':
                return status, reply
        statement = create_insert_statement('md_relations', [item['name'] for item in md_relations], md_relations)
        con = get_db_connection()
        cur = con.cursor()
        cur.execute(statement, (dataset['id'], json.dumps(relations)))
        con.commit()
        release_db_connection(con)

    return '1200', {}


def add_authorization():
    key = ''.join(secrets.choice(string.ascii_letters + string.digits) for i in range(16))
    statement = 'INSERT INTO md_authorization_key (key,expirationDate,operationsList) '
    statement += 'VALUES (' + ' , '.join([get_db_placeholder() for item in md_authorization_key])
    statement += ' );'
    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, (key, '2030-01-01T00:00:00Z', 'add_dataset,remove_dataset,add_data,remove_data',))
    con.commit()
    release_db_connection(con)
    logging.info('added authorization key: ' + key)


def is_authorized(key, method):
    statement = 'SELECT expirationDate,operationsList FROM md_authorization_key WHERE key = '+ get_db_placeholder()
    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, (key,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    if len(rows) == 0:
        logging.error('invalid key')
        return False

    # TODO - check expiration data and method
    return True


def add_data(data, args, replace=False):
    allowed_args = ['key', 'resample']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}
    if 'key' in args:
        key = args['key']
    else:
        logging.error('required parameter missing: key')
        return '1400', {}
    if not is_authorized(key, 'add_data'):
        logging.error('not authorized')
        return '1400', {}
    if 'dataset' in data:
        id = args['dataset']
    elif 'id' in data: # deprecated
        id = data['id']
    else:
        logging.error('required parameter missing: dataset')
        return '1400', {}
    if 'parameters' in data:
        parameters = data['parameters']
    else:
        logging.error('required parameter missing: {}'.format('parameters'))
        return '1400', {}

    resample = False
    if 'resample' in args:
        resample = False

    con = get_db_connection()
    cur = con.cursor()
    statement = 'SELECT relations FROM md_relations WHERE id = '+ get_db_placeholder()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    has_link = False
    if len(rows) != 0:
        relations = json.loads(rows[0][0])
        for relation in relations:
            if relation['type'] == 'link':
                has_link = True
                server = relation['server']
                source_id = relation['id']

    if not has_link:
        if 'data' in data:
            data2 = data['data']
        else:
            logging.error('required parameter missing: {}'.format('data'))
            return '1400', {}

        tuples = []
        times = []
        for item in data2:
            tuples.append(tuple(item))
            times.append((item[0],))

        con = get_db_connection()
        cur = con.cursor()
        statement = 'SELECT id FROM md_dataset WHERE id = ' + get_db_placeholder()
        cur.execute(statement, (id,))
        rows = cur.fetchall()
        if rows == []:
            return '1406', {}

        # remove existing data (why is POSTGRES replace so difficult)
        # statement = 'DELETE FROM ' + id + ' WHERE time = ' + get_db_placeholder()
        # cur.executemany(statement, times)

        # insert the data
        statement = create_insert_statement(id, parameters, [{'name': 'time', 'key': True}], replace=replace)
        cur.executemany(statement, tuples)
        logging.info('added ' + str(len(tuples)) + ' rows to dataset ' + id)

        statement = 'SELECT MIN(time), MAX(time) FROM ' + id
        cur.execute(statement)
        rows = cur.fetchall()
        con.commit()
        release_db_connection(con)
        startDate = rows[0][0] if rows[0][0] is not None else ''
        stopDate = rows[0][1] if rows[0][1] is not None else ''
    else:
        url = server + '/info?id=' + source_id
        response = urllib.request.urlopen(url)
        if response.status != 200:
            return '1500', {}
        info = json.loads(response.read())
        startDate = info['startDate']
        stopDate = info['stopDate']

    modificationDate = datetime.today().strftime(ISO_TIMEFMT)

    con = get_db_connection()
    cur = con.cursor()
    statement = 'UPDATE md_dataset SET '
    statement += 'modificationDate = ' + get_db_placeholder()
    statement += ', startDate = ' + get_db_placeholder()
    statement += ', stopDate = ' + get_db_placeholder()
    statement += ' WHERE id = ' + get_db_placeholder()
    cur.execute(statement, (modificationDate, startDate, stopDate, id))
    con.commit()
    release_db_connection(con)

    if resample:
        status, reply = resample_data(id, data, args)
        if status != '1200':
            return status, reply

    return '1200', {}


def get_data(args):
    allowed_args = ['dataset', 'start', 'stop', 'id', 'time.min', 'time.max', 'parameters', 'include', 'format']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', 'json', {}, {}
    if 'dataset' in args:
        id = args['dataset']
    elif 'id' in args: # deprecated
        id = args['id']
    else:
        logging.error('required parameter missing: dataset')
        return '1400', {}
    if 'start' in args:
        start = args['start']
    elif 'time.min' in args: # deprecated
        start = args['time.min']
    else:
        logging.error('required parameter missing: start')
        return '1400', 'json', {}, {}
    if 'stop' in args:
        stop = args['stop']
    elif 'time.max' in args: # deprecated
        stop = args['time.max']
    else:
        logging.error('required parameter missing: stop')
        return '1400', 'json', {}, {}

    parameters_request = []
    if 'parameters' in args:
        parameters_request = args['parameters'].split(',')

    include = ''
    if 'include' in args:
        include = args['include']

    format = 'csv'
    if 'format' in args:
        format = args['format']
    if format not in ['json', 'csv']:
        logging.error('invalid parameter: format: ' + format)
        return '1400', 'json', {}, {}

    try:
        start = pd.to_datetime(start, utc=True)
    except:
        logging.error('invalid parameter: start: ' + start)
        return '1402', 'json', {}, {}

    try:
        stop = pd.to_datetime(stop, utc=True)
    except:
        logging.error('invalid parameter: stop: ' + stop)
        return '1403', 'json', {}, {}

    start = start.strftime(ISO_TIMEFMT)
    stop = stop.strftime(ISO_TIMEFMT)

    names = []
    for item in md_parameter:
        names.append(item['name'])

    statement = 'SELECT '
    statement += ', '.join(names)
    statement += ' FROM md_parameter WHERE datasetID = ' + get_db_placeholder()

    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    if len(rows) == 0:
        return '1406', 'json', {}, {}

    parameters = []
    parameter_names = []
    default_time_parameter = ''
    for row in rows:
        parameter = {}
        for i in range(len(names)):
            parameter[names[i]] = list(row)[i]
        # the first isotime is the default time parameter
        if parameter['type'] == 'isotime' and default_time_parameter == '':
            default_time_parameter = parameter['name']
        if parameters_request == [] or parameter['name'] in parameters_request:
            parameter_names.append(parameter['name'])
            parameters.append(parameter)

    # check if a time paramter is specified, if not, add it
    time_index = -1
    for i, parameter in enumerate(parameter_names):
        if parameter == default_time_parameter:
            time_index = i
    if time_index == -1:
        parameter_names.append(default_time_parameter)

    con = get_db_connection()
    cur = con.cursor()
    statement = 'SELECT relations FROM md_relations WHERE id = '+ get_db_placeholder()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    has_link = False
    if len(rows) != 0:
        server = ''
        source_id = ''
        relations = json.loads(rows[0][0])
        for relation in relations:
            if relation['type'] == 'link':
                has_link = True
                server = relation['server']
                source_id = relation['id']

    if has_link:
        url = server + '/data?id=' + source_id + '&time.min=' + start + '&time.max=' + stop + '&format=json'
        response = urllib.request.urlopen(url)
        if response.status == 200:
            data = json.loads(response.read())
        rows = []
        for item in data['data']:
            rows.append(tuple(item))
    else:
        statement = 'SELECT ' + ', '.join(parameter_names) + ' FROM ' + id + ' WHERE ' + default_time_parameter + ' >= ' + get_db_placeholder()
        statement += ' AND ' + default_time_parameter + ' < ' + get_db_placeholder()
        statement += ' ORDER BY ' + default_time_parameter

        con = get_db_connection()
        cur = con.cursor()
        cur.execute(statement, (start, stop))
        rows = cur.fetchall()
        con.commit()
        release_db_connection(con)

    if format == 'json':
        data = []
        for row in rows:
            data.append(list(row))
        reply_header = {}
        reply = {}
        reply['parameters'] = parameters
        reply['data'] = data
    else:
        df = rows_to_dataframe(rows, parameter_names, time_name=default_time_parameter)
        reply_header = {}
        if include == 'header':
            reply_header['parameters'] = parameters
        elif include == 'brief_header':
            reply_header = ','.join(parameter_names)
        reply = df.to_csv(index=True, date_format=ISO_TIMEFMT, float_format='%g', header=False)

    return '1200' if len(rows) > 0 else '1201', format, reply, reply_header


def remove_data(args):
    allowed_args = ['id', 'time.min', 'time.max', 'dataset', 'start', 'stop', 'key']
    for arg in args:
        if arg not in allowed_args:
            logging.error('invalid parameter: ' + arg)
            return '1401', {}
    if 'key' in args:
        key = args['key']
    else:
        logging.error('required parameter missing: key')
        return '1400', {}
    if not is_authorized(key, 'add_data'):
        logging.error('not authorized')
        return '1400', {}
    if 'dataset' in args:
        dataset = args['dataset']
    elif 'id' in args: # deprecated
        dataset= args['id']
    else:
        logging.error('required parameter missing: dataset')
        return '1400', {}
    if 'start' in args:
        start = args['start']
    elif 'time.min' in args: # deprecated
        start = args['time.min']
    else:
        logging.error('required parameter missing: start')
        return '1400', {}
    if 'stop' in args:
        stop = args['stop']
    elif 'time.max' in args: # deprecated
        stop = args['time.max']
    else:
        logging.error('required parameter missing: stop')
        return '1400', {}

    try:
        start = pd.to_datetime(start, utc=True)
    except:
        logging.error('invalid parameter: start: ' + start)
        return '1402', {}

    try:
        stop = pd.to_datetime(stop, utc=True)
    except:
        logging.error('invalid parameter: stop: ' + stop)
        return '1403', {}

    start = start.strftime(ISO_TIMEFMT)
    stop = stop.strftime(ISO_TIMEFMT)

    statement = 'SELECT id FROM md_dataset WHERE id = ' + get_db_placeholder()

    con = get_db_connection()
    cur = con.cursor()
    cur.execute(statement, (dataset,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    if len(rows) == 0:
        return '1406', {}

    modificationDate = datetime.today().strftime('%Y-%m-%dT%H:%M:%S.%f')

    con = get_db_connection()
    cur = con.cursor()
    statement = 'DELETE FROM ' + dataset + ' WHERE time >= ' + get_db_placeholder()
    statement += ' AND time < ' + get_db_placeholder()
    cur.execute(statement, (start, stop))
    statement = 'SELECT MIN(time), MAX(time) FROM ' + dataset
    cur.execute(statement)
    rows = cur.fetchall()
    startDate = rows[0][0] if rows[0][0] is not None else ''
    stopDate = rows[0][1] if rows[0][1] is not None else ''
    statement = 'UPDATE md_dataset SET '
    statement += 'modificationDate = ' + get_db_placeholder()
    statement += ', startDate = ' + get_db_placeholder()
    statement += ', stopDate = ' + get_db_placeholder()
    statement += ' WHERE id = ' + get_db_placeholder()
    cur.execute(statement, (modificationDate, startDate, stopDate, dataset))
    con.commit()
    release_db_connection(con)

    return '1200', {}


def resample_data(id, data, args):
    con = get_db_connection()
    cur = con.cursor()
    statement = 'SELECT relations FROM md_relations WHERE id = '+ get_db_placeholder()
    cur.execute(statement, (id,))
    rows = cur.fetchall()
    con.commit()
    release_db_connection(con)

    parameter_names = data['parameters']

    if len(rows) != 0:
        has_link = False
        server = ''
        source_id = ''
        relations = json.loads(rows[0][0])
        for relation in relations:
            if relation['type'] == 'link':
                has_link = True
                server = relation['server']
                source_id = relation['id']

        for relation in relations:
            if relation['type'] == 'resample':
                time_index = 0
                time_name = ''
                for i, parameter in enumerate(parameter_names):
                    if parameter == 'time' or parameter == 'Time':
                        time_index = i
                        time_name = parameter_names[i]

                data['id'] = relation['id']
                data2 = data['data']

                time_start = pd.to_datetime(data2[0][time_index], utc=True)
                time_stop = pd.to_datetime(data2[-1][time_index], utc=True)

                new_cadence = pd.to_timedelta(relation['cadence'])
                time_start = time_start - new_cadence
                time_stop = time_stop + new_cadence
                index = pd.date_range(start=time_start, end=time_stop, freq='30T')
                series = pd.Series(len(index), index=index)
                series_resampled = series.resample(new_cadence, origin='epoch').mean()
                time_start = pd.to_datetime(series_resampled.index.values[0], utc=True).strftime(ISO_TIMEFMT)
                time_stop = pd.to_datetime(series_resampled.index.values[-1], utc=True).strftime(ISO_TIMEFMT)

                if has_link:
                    url = server + '/data?id=' + source_id + '&time.min=' + time_start + '&time.max=' + time_stop + '&format=json'
                    response = urllib.request.urlopen(url)
                    if response.status == 200:
                        data = json.loads(response.read())
                        tuples = []
                        for item in data['data']:
                            tuples.append(tuple(item))
                        df = rows_to_dataframe(tuples, parameter_names, time_name=time_name)
                else:
                    statement = 'SELECT * FROM ' + id + ' WHERE ' + time_name + ' >= ' + get_db_placeholder()
                    statement += ' AND ' + time_name + ' < ' + get_db_placeholder()
                    statement += ' ORDER BY ' + time_name

                    con = get_db_connection()
                    cur = con.cursor()
                    cur.execute(statement, (time_start, time_stop))
                    rows = cur.fetchall()
                    con.commit()
                    release_db_connection(con)

                    df = rows_to_dataframe(rows, parameter_names, time_name=time_name)

                new_cadence = pd.to_timedelta(relation['cadence'])
                resample_method = relation['method']

                if resample_method == 'max':
                    df = df.apply(pd.to_numeric)
                    df_resampled = df.resample(new_cadence, origin='epoch').max()
                elif resample_method == 'min':
                    df = df.apply(pd.to_numeric)
                    df_resampled = df.resample(new_cadence, origin='epoch').min()
                elif resample_method == 'mean':
                    df = df.apply(pd.to_numeric)
                    df_resampled = df.resample(new_cadence, origin='epoch').mean()
                elif resample_method == 'first':
                    df_resampled = df.resample(new_cadence, origin='epoch').first()
                elif resample_method == 'last':
                    df_resampled = df.resample(new_cadence, origin='epoch').last()
                else:
                    logging.error("Unknown resampling method specification: " f"{resample_method}")
                    continue

                # Pandas assigns the leftmost time-tag of the resampling interval
                #   to the new index. We should add half the interval, to put the
                #   new time tags at the middle of the resampling interval.
                new_timeindex = df_resampled.index + 0.5 * new_cadence
                time = new_timeindex.strftime(ISO_TIMEFMT)
                df_resampled.insert(0, 'time', time)

                dataContainerResampled = {}
                dataContainerResampled['id'] = relation['id']
                dataContainerResampled['parameters'] = parameter_names
                dataContainerResampled['data'] = df_resampled.values.tolist()

                status, reply = add_data(dataContainerResampled, {'key': args['key']}, replace=True)
                if status != '1200':
                    return status, reply

    return '1200', {}

