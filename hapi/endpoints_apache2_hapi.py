from flask import Flask, request, Response
from hapi import hapi_3_1_0 as hapi
from flask_cors import CORS, cross_origin

application = Flask(__name__)
CORS(application)

# ------------------------------------------------------------------------------
# HAPI endpoints
# ------------------------------------------------------------------------------

@application.route('/about', methods=['GET'])
@cross_origin()
def hapi_about():
    response, status, mimetype = hapi.hapi_about(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/capabilities', methods=['GET'])
@cross_origin()
def hapi_capabilities():
    response, status, mimetype = hapi.hapi_capabilities(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/catalog', methods=['GET'])
@cross_origin()
def hapi_catalog():
    response, status, mimetype = hapi.hapi_catalog(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/info', methods=['GET'])
@cross_origin()
def hapi_info():
    response, status, mimetype = hapi.hapi_info(request.args)
    return Response(response, status=status, mimetype=mimetype)

@application.route('/data', methods=['GET'])
@cross_origin()
def hapi_data():
    response, status, mimetype = hapi.hapi_data(request.args)
    return Response(response, status=status, mimetype=mimetype)

